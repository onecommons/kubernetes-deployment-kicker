# /usr/bin/env python

"""
External heath checker & pod kicker

Restarts multiple Kubernetes deployments if a healthcheck fails.
For those services that depend on each other, but can't be modifed to retry.
"""

import yaml
import datetime
import logging
import requests
import os
import kubernetes as k8s


logging.basicConfig(
    format="%(levelname)7s :: %(message)s",
    level=os.environ.get("LOG_LEVEL", "INFO").upper(),
)
# disable k8s client debug logging (the whole response json is too verbose)
logging.getLogger("kubernetes.client.rest").setLevel("INFO")


def restart_deployment(kapi, deployment, namespace):
    # update `spec.template.metadata` section to add
    # `kubectl.kubernetes.io/restartedAt` annotation
    now = datetime.datetime.utcnow().isoformat()
    body = {
        "spec": {
            "template": {
                "metadata": {"annotations": {"kubectl.kubernetes.io/restartedAt": now}}
            }
        }
    }

    # patch deployment
    try:
        kapi.patch_namespaced_deployment(deployment, namespace, body)
    except k8s.ApiException as e:
        logging.error(f"{deployment}: failed to restart: {e}")


def coerce_deployment(thing: str | dict) -> dict:
    match thing:
        # assume dict is already in the format we want
        case d if type(thing) is dict:
            if "namespace" not in d:
                # if no namespace, die
                logging.error(f"namespace must be included in {thing}")
                exit(1)

            return d

        # split combo string on /
        case s if "/" in s:
            ns, n = s.split("/")
            return {"namespace": ns, "name": n}
        # if no /, no namespace, die
        case s:
            logging.error(f"namespace must be included in {thing}")
            exit(1)


def main():
    # read config file
    filename = os.environ.get("CONFIG_FILE", default="config.yaml")
    try:
        with open(filename) as cfgfile:
            CONFIG = yaml.safe_load(cfgfile)
    except IOError:
        logging.error(f"config file '{filename}' not found! is the configmap set?")
        exit(1)

    logging.debug("loading kubeconfig")
    k8s.config.load_config()

    for name, opts in CONFIG.items():
        logging.info(f"{name} :: checking service")

        # extract namespace from deployments config
        deployments = [coerce_deployment(d) for d in opts["deployments"]]

        # is it broke?
        logging.debug(f"{name} :: fetching {opts['check']['url']}")

        try:
            resp = requests.get(opts["check"]["url"])
            logging.debug(f"{name} :: check returned {resp.status_code}")

            if not resp.status_code == requests.codes.ok:
                # requests will raise for other errors e.g. DNS, so match that here
                raise "request failed!"

            logging.debug(f"{name} :: service is ok")

        except BaseException as e:
            logging.warning(f"{name} :: check failed! restarting deployments...")
            logging.debug(f"{name} :: reason: {e}")
            # try to fix
            apps_v1 = k8s.client.AppsV1Api()
            for d in deployments:
                logging.debug(f"{name} :: restarting {d['namespace']}/{d['name']}")
                restart_deployment(apps_v1, d["name"], d["namespace"])


if __name__ == "__main__":
    main()
