# Kubernetes Deployment Kicker

Simple K8S job / script to restart ("kick over") deployments if a health-check style request fails.

`registry.gitlab.com/onecommons/kubernetes-deployment-kicker:main`

## Setup

There are two options to deploy the kicker: cluster-wide or per-namespace.

### 1. Cluster-wide (`kicker-cluster.manifest.yaml`)

This deploys the kicker in a dedicated namespace (`kicker`) with cluster-wide permissions to restart any deployment.

```sh
kubectl apply -f kicker-cluster.manifest.yaml
```

### 2. Namespaced (`kicker.manifest.yaml`)

This deploys the kicker in a specific namespace (`example`, here) with permissions only in that namespace.

```sh
kubectl apply --namespace example -f kicker.manifest.yaml
```

## Config

You will need to create the required configuration configmap (make sure namespace matches the option you chose):

```sh
cp config.example.yaml config.yaml

$EDITOR config.yaml # Add services to config.yaml as desired!

kubectl create --namespace <kicker|example> configmap kicker-config --from-file=config.yaml
```

The format of `config.yaml` is:

```yaml
$NAME:
  check:
    url: $ENDPOINT
  deployments:
    - $DEPLOYMENT
```

- `$NAME`: shows in logging, can be anything
- `$ENDPOINT`: the healthcheck URL -- if requests to this fail (i.e. 400/500 errors), the deployment(s) should be restarted.

   Can be any URL, but you probably want this to be the Service endpoint for the deployment.

- `$DEPLOYMENT`: the deployments to restart if the healthcheck fails. This can either be a kubectl-style string, or a manifest-style yaml block:

    ```yaml
    deployments:
      - foo/bar
      - namespace: foo
        name: bar
    ```
